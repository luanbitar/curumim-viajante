"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp(functions.config().firebase);
exports.updatedUser = functions.firestore.document('/users/{id}').onUpdate((change, context) => {
    const newValue = change.after.data();
    const previousValue = change.before.data();
    if (newValue.photoURL === previousValue.photoURL) {
        console.log('photoURL doesnt changed.');
        return null;
    }
    return admin.firestore().collection('ticket').where('owner.id', '==', newValue.uid).get().then((obj) => {
        console.log('Update', obj.size);
        obj.forEach((result) => {
            return result.ref.set({ owner: { photoURL: newValue.photoURL } }, { merge: true });
        });
    });
});
//# sourceMappingURL=index.js.map