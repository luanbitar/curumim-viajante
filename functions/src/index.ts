import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { OutgoingMessage } from 'http';

admin.initializeApp(functions.config().firebase);

exports.updatedUser = functions.firestore.document('/users/{id}').onUpdate((change, context) => {
  const newValue = change.after.data();
  const previousValue = change.before.data();

  if (newValue.photoURL === previousValue.photoURL) {
    console.log('photoURL doesnt changed.');
    return null;
  }
  return admin.firestore().collection('ticket').where('owner.id', '==', newValue.uid).get().then((obj) => {
    console.log('Update', obj.size);
    obj.forEach((result) => {
      return result.ref.set({ owner: { photoURL: newValue.photoURL } }, { merge: true });
    });
  });
});

