export class HandleErrorUtils {
    public static message(code: string): string {
        let errorMessage = '';
        switch (code) {
        case 'auth/account-exists-with-different-credential':
            errorMessage = 'Este e-mail já está sendo usado';
            break;
        case 'auth/wrong-password':
            errorMessage = 'Senha inválida';
            break;
        case 'auth/user-not-found':
            errorMessage = 'Usuário não encontrado';
            break;
        case 'auth/user-disabled':
            errorMessage = 'Usuário não habilitado';
            break;
        case 'auth/invalid-email':
            errorMessage = 'E-mail inválido';
            break;
        case 'auth/network-request-failed':
            errorMessage = 'Sem conexão com internet';
            break;
        default:
            errorMessage = 'Algo deu errado';
        }
        return errorMessage;
    }
}
