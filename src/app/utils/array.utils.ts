export class ArrayUtils {
  public static preventRepeatedValues(array: Array<any>, key?: string): Array<any> {
    if (array) {
      if (key) {
        array = array.map(option => option[key]);
      }
      array = [...new Set(array)];
    }
    return array;
  }
}
