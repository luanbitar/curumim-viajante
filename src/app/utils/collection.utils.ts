import { AngularFirestoreCollection } from 'angularfire2/firestore';

export class CollectionUtils {
  public static aggregate(
    collection: AngularFirestoreCollection<any>,
    keys: Array<Array<string>>
  ): Array<any> {
    const result = [];
    collection.ref.get().then((mainQuery) => {
      mainQuery.forEach(mainElement => {
        const dataRef = mainElement.data();
        keys.forEach(key => {
          for (const dataKey in dataRef) {
            if (dataKey === key[0]) {
              dataRef[dataKey].get().then(refQuery => {
                dataRef[key[1]] = refQuery.data();
                delete dataRef[dataKey];
              });
            }
          }
        });
        result.push(dataRef);
      });
    });
    return result;
  }
}
