import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientRoutingModule } from './client-routing.module';
import { ClientDashboardComponent } from './client-dashboard/client-dashboard.component';
import { ClientNavbarComponent } from './client-navbar/client-navbar.component';
import { ClientDashboardFilterComponent } from './client-dashboard/client-dashboard-filter/client-dashboard-filter.component';
import { SharedModule } from '../shared/shared.module';
import { UsersModule } from '../users/users.module';

@NgModule({
  imports: [
    CommonModule,
    ClientRoutingModule,
    SharedModule,
    UsersModule
  ],
  declarations: [
    ClientDashboardComponent,
    ClientNavbarComponent,
    ClientDashboardFilterComponent
  ]
})
export class ClientModule { }
