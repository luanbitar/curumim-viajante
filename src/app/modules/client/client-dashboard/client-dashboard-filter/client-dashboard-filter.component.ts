import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subscription, Subject } from 'rxjs';

import { TicketsService, Ticket } from '../../../shared/services/tickets.service';
import { ArrayUtils } from '../../../../utils/array.utils';

export interface DestinationEmit {
  destination: string;
  options: Array<string>;
}
@Component({
  selector: 'app-client-dashboard-filter',
  templateUrl: './client-dashboard-filter.component.html',
  styleUrls: ['./client-dashboard-filter.component.scss']
})
export class ClientDashboardFilterComponent implements OnInit, OnDestroy {

  @Output('destinationChanged') destinationChanged: EventEmitter<DestinationEmit> = new EventEmitter<DestinationEmit>();

  public rootInput: FormControl = new FormControl();
  public destinationInput: FormControl = new FormControl();
  public filteredRootOptions: Array<Ticket>;
  public filteredDestinationOptions: Array<Ticket>;
  private subscriptions = new Subscription();
  private searchRoot = new Subject<string>();
  private searchDestination = new Subject<string>();

  constructor(private ticketsService: TicketsService) {
    this.subscriptions = this.ticketsService.getRootTickets(this.searchRoot).subscribe(tickets => {
      this.filteredRootOptions = tickets;
    });
    this.subscriptions.add(this.ticketsService.getDestinationTickets(this.searchDestination).subscribe(tickets => {
      this.filteredDestinationOptions = tickets;
    }));
  }

  ngOnInit() {
    this.setInputsListeners();
  }

  private setInputsListeners(): void {
    this.searchRoot.next('');
    this.searchDestination.next('');
    this.subscriptions.add(
      this.rootInput.valueChanges.subscribe(val => {
        this.searchRoot.next(val);
      })
    );
    this.subscriptions.add(
      this.destinationInput.valueChanges.subscribe(val => {
        this.destinationChanged.emit({
          destination: val,
          options: this.arrayDestination
        });
        this.searchDestination.next(val);
      })
    );
  }

  public get arrayDestination(): Array<string> {
    return ArrayUtils.preventRepeatedValues(this.filteredRootOptions, 'destination');
  }
  public get arrayRoot(): Array<string> {
    return ArrayUtils.preventRepeatedValues(this.filteredRootOptions, 'root');
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
