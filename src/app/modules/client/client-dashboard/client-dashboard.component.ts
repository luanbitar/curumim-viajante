import { Component, OnInit, OnDestroy } from '@angular/core';

import { AngularFirestore } from 'angularfire2/firestore';
import { Subscription } from 'rxjs';

import { CollectionUtils } from '../../../utils/collection.utils';
import { TicketsService, Ticket } from '../../shared/services/tickets.service';
import { DestinationEmit } from './client-dashboard-filter/client-dashboard-filter.component';

@Component({
  selector: 'app-client-dashboard',
  templateUrl: './client-dashboard.component.html',
  styleUrls: ['./client-dashboard.component.scss']
})
export class ClientDashboardComponent implements OnInit, OnDestroy {

  public tickets: Array<Ticket>;
  public destinationFilteredTickets: Array<Ticket>;
  private subscriptions = new Subscription();

  constructor(
    private db: AngularFirestore,
    private ticketsService: TicketsService
  ) {}

  ngOnInit() {
    this.setTicketListener();
  }

  private setTicketListener(): void {
    this.subscriptions = this.ticketsService.rootTickets.subscribe(tickets => {
      this.destinationFilteredTickets = tickets;
      this.tickets = tickets;
    });
  }

  public filterByDestination(destinationEmit: DestinationEmit): void {
    this.destinationFilteredTickets =
    destinationEmit.options.indexOf(destinationEmit.destination) > -1
    ? this.tickets.filter(ticket => ticket.destination === destinationEmit.destination)
    : this.tickets;
  }

  private howToUseCollectionUtils(): any[] {
    return CollectionUtils.aggregate(
      this.db.collection('ticket'),
      [
        ['uid', 'users'],
        ['testId', 'collectionstest']
      ]
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
