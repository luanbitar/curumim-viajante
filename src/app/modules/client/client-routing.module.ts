import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClientNavbarComponent } from './client-navbar/client-navbar.component';
import { ClientDashboardComponent } from './client-dashboard/client-dashboard.component';
import { AuthGuard } from '../auth/guards/auth.guard';
import { ProfileEditComponent } from '../users/profile-edit/profile-edit.component';

const routes: Routes = [
  {
    path: '',
    component: ClientNavbarComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: ClientDashboardComponent,
        canActivateChild: [AuthGuard]
      },
      {
        path: 'perfil/editar',
        component: ProfileEditComponent,
        canActivateChild: [AuthGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientRoutingModule { }
