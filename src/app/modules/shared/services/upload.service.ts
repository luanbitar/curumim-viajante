import { Injectable } from '@angular/core';

import * as firebase from 'firebase/app';

import { Upload } from '../models/Upload';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  private basePath = '/uploads';

  constructor() { }

  pushUpload(upload: Upload, callback: Function) {
    const storageRef = firebase.storage().ref();
    const uploadTask = storageRef.child(`${this.basePath}/${upload.file.name}`).put(upload.file);
    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot: any) => {
        // upload in progress
        upload.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      },
      (error) => {
        // upload failed
      },
      () => {
        // then
        uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) => {
          upload.url = downloadURL;
          upload.name = upload.file.name;
          callback(upload);
        });
      }
    );
  }
}
