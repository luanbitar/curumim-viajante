import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarRef, SimpleSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  public readonly options = [
    'success',
    'danger',
    'warning'
  ];

  constructor(
    private snackBar: MatSnackBar,
  ) { }

  public open(message: string, type = 'success'): MatSnackBarRef<SimpleSnackBar> {
    return this.snackBar.open(message, '', {
      duration: 3000,
      panelClass: `alert-${type}`
    });
  }
}
