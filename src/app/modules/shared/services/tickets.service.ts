import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from 'angularfire2/firestore';
import { Subject, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

export interface Ticket {
  title: string;
  description: string;
  price: number;
  root: string;
  destination: string;
  banner: string;
  owner: {
    id: string;
    photoURL: string;
  };
}

@Injectable({
  providedIn: 'root'
})
export class TicketsService {

  private collection = 'ticket';
  public rootTickets = new Observable<Ticket[]>();
  public ownerTickets = new Observable<Ticket[]>();

  constructor(private db: AngularFirestore) { }

  public getRootTickets(start: Subject<string>): Observable<Ticket[]> {
    this.rootTickets = this.getTickets(start, 'root');
    return this.rootTickets;
  }
  public getDestinationTickets(start: Subject<string>): Observable<Ticket[]> {
    return this.getTickets(start, 'destination');
  }

  private getTickets(start: Subject<string>, property: string): Observable<Ticket[]> {
    return start.pipe(switchMap(st => {
      if (st !== null && st !== '' && st !== undefined) {
        return this.db.collection<Ticket>(this.collection, list => list
        .orderBy(property)
        .startAt(st)
        .endAt(st + '\uF8FF'))
        .valueChanges();
      } else {
        return this.db.collection<Ticket>(this.collection).valueChanges();
      }
    }));
  }

  public getOwnerTickets(uid: string, destination: Subject<string>): Observable<Ticket[]> {
    this.ownerTickets = destination.pipe(switchMap(dest => {
      if (dest !== undefined && dest !== null && dest !== '') {
        return this.db.collection<Ticket>(this.collection, list => list
        .where('owner.id', '==', uid)
        .orderBy('destination')
        .startAt(dest)
        .endAt(dest + '\uF8FF'))
        .valueChanges();
      } else {
        return this.db.collection<Ticket>(this.collection, list => list
        .where('owner.id', '==', uid))
        .valueChanges();
      }
    }));
    return this.ownerTickets;
  }

  public saveTicket(ticket: Ticket): Promise<DocumentReference> {
    return this.db.collection(this.collection).add(ticket);
  }
}
