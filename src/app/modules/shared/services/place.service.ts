import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';

import { environment } from './../../../../environments/environment';
import { State, City } from '../models/Places';

@Injectable({
  providedIn: 'root'
})
export class PlaceService {

  private baseUrl = environment.placeUrl;
  private _states: Array<State>;

  constructor(private http: HttpClient) { }

  public get states(): Observable<Array<State>> {
    return Observable.create(observer => {
      if (!this._states) {
        this._states = new Array<State>();
        this.http.get<Array<State>>(`${this.baseUrl}/estados`).subscribe(states => {
          this._states = states;
          observer.next(states);
        });
      } else {
        observer.next(this._states);
      }
    });
  }

  public getCities(UF: number): Observable<Array<City>> {
    return this.http.get<Array<City>>(`${this.baseUrl}/estados/${UF}/municipios`);
  }

  public getStateFromCity(city: number): Observable<State> {
    return this.http.get<State>(`${this.baseUrl}/municipios/${city}`);
  }
}
