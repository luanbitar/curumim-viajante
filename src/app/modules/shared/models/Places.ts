interface Base {
  id: number;
  nome: string;
}
interface Regiao extends Base {
  sigla: string;
}
interface MicroRegiao extends Base {
  mesoregiao: MesoRegiao;
}
interface MesoRegiao extends Base {
  UF: State;
}

export interface State extends Regiao {
  regiao: Regiao;
}

export interface City extends Base {
  microregiao: MicroRegiao;
}


