import { Component, OnInit, OnDestroy } from '@angular/core';

import { Subscription } from 'rxjs';

import { DestinationEmit } from './../../client/client-dashboard/client-dashboard-filter/client-dashboard-filter.component';
import { Ticket, TicketsService } from '../../shared/services/tickets.service';

@Component({
  selector: 'app-seller-dashboard',
  templateUrl: './seller-dashboard.component.html',
  styleUrls: ['./seller-dashboard.component.scss']
})
export class SellerDashboardComponent implements OnInit, OnDestroy {

  public ownerTickets: Array<Ticket>;
  public destinationFilteredOwnerTickets: Array<Ticket>;
  private subscriptions = new Subscription();

  constructor(
    private ticketsService: TicketsService
  ) {}

  ngOnInit() {
    this.setTicketListener();
  }

  private setTicketListener(): void {
    this.subscriptions = this.ticketsService.ownerTickets.subscribe(ownerTickets => {
      this.destinationFilteredOwnerTickets = ownerTickets;
      this.ownerTickets = ownerTickets;
    });
  }

  public filterByDestination(destinationEmit: DestinationEmit): void {
    this.destinationFilteredOwnerTickets =
    destinationEmit.options.indexOf(destinationEmit.destination) > -1
    ? this.ownerTickets.filter(ticket => ticket.destination === destinationEmit.destination)
    : this.ownerTickets;
  }

  ngOnDestroy() { this.subscriptions.unsubscribe(); }
}
