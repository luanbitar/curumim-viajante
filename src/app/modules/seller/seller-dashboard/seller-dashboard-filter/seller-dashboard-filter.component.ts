import { Component, OnInit, Output, EventEmitter, OnDestroy, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

import { Subject, Subscription } from 'rxjs';

import { DestinationEmit } from './../../../client/client-dashboard/client-dashboard-filter/client-dashboard-filter.component';
import { ArrayUtils } from '../../../../utils/array.utils';
import { Ticket, TicketsService } from '../../../shared/services/tickets.service';
import { AuthService } from './../../../auth/auth.service';

@Component({
  selector: 'app-seller-dashboard-filter',
  templateUrl: './seller-dashboard-filter.component.html',
  styleUrls: ['./seller-dashboard-filter.component.scss']
})
export class SellerDashboardFilterComponent implements OnInit, OnDestroy {

  public destinationInput: FormControl = new FormControl();
  public filteredOwnerOptions: Array<Ticket>;
  private subscriptions = new Subscription();
  private destinationEmitter = new Subject<string>();

  constructor(
    private ticketsService: TicketsService,
    private authService: AuthService) {
    this.subscriptions = this.authService.user.subscribe(user => {
      this.subscriptions.add(
        this.ticketsService.getOwnerTickets(user.uid, this.destinationEmitter).subscribe(tickets => {
          this.filteredOwnerOptions = tickets;
        })
      );
    });
  }

  ngOnInit() {
    this.setInputsListeners();
  }

  private setInputsListeners(): void {
    this.destinationEmitter.next('');
    this.subscriptions.add(
      this.destinationInput.valueChanges.subscribe(val => {
        this.destinationEmitter.next(val);
      })
    );
  }

  public get arrayDestination(): Array<string> {
    return ArrayUtils.preventRepeatedValues(this.filteredOwnerOptions, 'destination');
  }

  ngOnDestroy() { this.subscriptions.unsubscribe(); }
}
