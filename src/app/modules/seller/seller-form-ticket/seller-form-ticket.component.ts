import { Component, OnInit, DoCheck, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { FormBuilder, Validators, AbstractControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { Subscription } from 'rxjs';

import { City } from './../../shared/models/Places';
import { PlaceService } from '../../shared/services/place.service';
import { AuthService } from '../../auth/auth.service';
import { TicketsService } from '../../shared/services/tickets.service';
import { AlertService } from './../../shared/services/alert.service';

@Component({
  selector: 'app-seller-form-ticket',
  templateUrl: './seller-form-ticket.component.html',
  styleUrls: ['./seller-form-ticket.component.scss']
})
export class SellerFormTicketComponent implements OnInit, DoCheck, OnDestroy {

  public ticketForm = this.fb.group({
    title: ['', Validators.required],
    description: ['', Validators.required],
    price: ['', Validators.required],
    rootGroup: this.place,
    destinationGroup: this.place,
    root: '',
    destination: '',
    banner: ['', Validators.required],
    owner: this.fb.group({
      id: [''],
      photoURL: [''],
    })
  });
  public rootCities = new Array<City>();
  public destinationCities = new Array<City>();
  private subscriptions = new Subscription();
  public isLoading = {
    onSubmit: false,
    rootCities: false,
    destinationCities: false
  };

  constructor(
    private fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private router: Router,
    public placeService: PlaceService,
    private authService: AuthService,
    private ticketService: TicketsService,
    private alertService: AlertService
  ) {
    this.subscriptions = this.rootState.valueChanges.subscribe(state => this.loadCountries(state.id, 'root'));
    this.subscriptions.add(this.destinationState.valueChanges.subscribe(state => this.loadCountries(state.id, 'destination')));
  }

  private get place(): FormGroup {
    return this.fb.group({
      state: ['', Validators.required],
      city: [{ value: '', disabled: true }, Validators.required],
    });
  }

  public ngOnInit(): void {}
  public ngDoCheck(): void { this.cdr.detectChanges(); }

  public get title(): AbstractControl { return this.ticketForm.get('title'); }
  public get price(): AbstractControl { return this.ticketForm.get('price'); }
  public get banner(): AbstractControl { return this.ticketForm.get('banner'); }
  public get description(): AbstractControl { return this.ticketForm.get('description'); }
  public get rootState(): AbstractControl { return this.ticketForm.get('rootGroup').get('state'); }
  public get rootCity(): AbstractControl { return this.ticketForm.get('rootGroup').get('city'); }
  public get destinationState(): AbstractControl { return this.ticketForm.get('destinationGroup').get('state'); }
  public get destinationCity(): AbstractControl { return this.ticketForm.get('destinationGroup').get('city'); }
  public get userImage(): string | null { return JSON.parse(localStorage.getItem('user')).photoURL; }
  public get root(): string { return `${this.rootCity.value} - ${this.rootState.value.sigla ? this.rootState.value.sigla : ''}`; }
  public get destination(): string {
    return `${this.destinationCity.value} - ${this.destinationState.value.sigla ? this.destinationState.value.sigla : ''}`;
  }

  private loadCountries(stateID: number, place: string): void {
    if (place === 'root') {
      this.isLoading.rootCities = true;
    } else if (place === 'destination') {
      this.isLoading.destinationCities = true;
    }
    this.subscriptions.add(this.placeService.getCities(stateID).subscribe(cities => {
      if (place === 'root') {
        this.rootCities = cities;
        this.isLoading.rootCities = false;
        if (cities.length > 0) {
          this.rootCity.enable();
        } else {
          this.rootCity.disable();
        }
      } else if (place === 'destination') {
        this.destinationCities = cities;
        this.isLoading.destinationCities = false;
        if (cities.length > 0) {
          this.destinationCity.enable();
        } else {
          this.destinationCity.disable();
        }
      }
    }));
  }

  public saveTicket(): void {
    if (this.ticketForm.invalid) { return; }
    this.isLoading.onSubmit = true;
    this.handleSubmitData();
    this.ticketService.saveTicket(this.ticketForm.value)
    .then(() => {
      this.alertService.open('Passagem salva com sucesso', 'success');
      this.router.navigate(['/seller']);
      this.isLoading.onSubmit = false;
    }).catch(() => {
      this.isLoading.onSubmit = false;
    });
  }

  private handleSubmitData(): void {
    this.subscriptions.add(this.authService.user.subscribe(userData => {
      this.ticketForm.patchValue({
        root: this.root,
        destination: this.destination,
        owner: {
          id: userData.uid,
          photoURL: userData.photoURL
        }
      });
      this.ticketForm.removeControl('destinationGroup');
      this.ticketForm.removeControl('rootGroup');
    }));
  }

  ngOnDestroy(): void { this.subscriptions.unsubscribe(); }
}
