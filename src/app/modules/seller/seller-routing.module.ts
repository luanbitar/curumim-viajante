import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SellerNavbarComponent } from './seller-navbar/seller-navbar.component';
import { AuthGuard } from '../auth/guards/auth.guard';
import { SellerDashboardComponent } from './seller-dashboard/seller-dashboard.component';
import { ProfileEditComponent } from '../users/profile-edit/profile-edit.component';
import { SellerFormTicketComponent } from './seller-form-ticket/seller-form-ticket.component';

const routes: Routes = [
  {
    path: '',
    component: SellerNavbarComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: SellerDashboardComponent,
        canActivateChild: [AuthGuard]
      },
      {
        path: 'perfil/editar',
        component: ProfileEditComponent,
        canActivateChild: [AuthGuard]
      },
      {
        path: 'passagem/cadastrar',
        component: SellerFormTicketComponent,
        canActivateChild: [AuthGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SellerRoutingModule { }
