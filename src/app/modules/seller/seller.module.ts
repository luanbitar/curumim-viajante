import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SellerRoutingModule } from './seller-routing.module';
import { SharedModule } from '../shared/shared.module';
import { SellerNavbarComponent } from './seller-navbar/seller-navbar.component';
import { SellerDashboardComponent } from './seller-dashboard/seller-dashboard.component';
import { UsersModule } from '../users/users.module';
import { SellerDashboardFilterComponent } from './seller-dashboard/seller-dashboard-filter/seller-dashboard-filter.component';
import { SellerFormTicketComponent } from './seller-form-ticket/seller-form-ticket.component';

@NgModule({
  imports: [
    CommonModule,
    SellerRoutingModule,
    SharedModule,
    UsersModule
  ],
  declarations: [
    SellerNavbarComponent,
    SellerDashboardComponent,
    SellerDashboardFilterComponent,
    SellerFormTicketComponent
  ]
})
export class SellerModule { }
