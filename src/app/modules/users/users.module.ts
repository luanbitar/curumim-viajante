import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { UsersRoutingModule } from './users-routing.module';
import { ProfileEditComponent } from './profile-edit/profile-edit.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    UsersRoutingModule,
    SharedModule
  ],
  declarations: [
    ProfileEditComponent
  ],
  exports: [
    ProfileEditComponent
  ]
})
export class UsersModule { }
