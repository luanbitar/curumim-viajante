import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';

import { environment } from './../../../../environments/environment';
import { User, AuthService } from './../../auth/auth.service';
import { Upload } from '../../shared/models/Upload';
import { UploadService } from '../../shared/services/upload.service';

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.scss']
})
export class ProfileEditComponent implements OnInit {


  @ViewChild('file') filePicker;
  public isUploading = false;

  public user: User;
  public profileForm = this.fb.group({
    displayName: [''],
    photoURL: ['']
  });

  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private router: Router,
    private uploadService: UploadService
  ) { }

  ngOnInit() {
    this.loadUser();
  }

  public get displayName(): AbstractControl { return this.profileForm.get('displayName'); }
  public get photoURL(): AbstractControl { return this.profileForm.get('photoURL'); }

  public imgPlaceholder(ev): void {
    ev.target.src = this.getAvatarUrl(this.photoURL.value);
  }

  private getAvatarUrl(value: string): string {
    return `${environment.avatarUrl}/avatars/200/${value.replace(/\//g, '')}`;
  }

  private loadUser(): void {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.profileForm.setValue({
      displayName: this.user.displayName ? this.user.displayName : '',
      photoURL: this.user.photoURL ? this.user.photoURL : ''
    });
  }

  public save(): void {
    const newImg = new Image;
    newImg.onerror = () => {
      this.photoURL.setValue(this.getAvatarUrl(this.photoURL.value));
      this.updateUserProfileData();
    };
    newImg.onload = () => {
      this.updateUserProfileData();
    };
    newImg.src = this.photoURL.value;
  }

  // TODO: handler de roles
  private updateUserProfileData(): void {
    this.router.navigate(['/client']);
    this.auth.updateUserData(this.user, this.profileForm.value);
  }

  public onUpload(event): void {
    this.isUploading = true;
    this.uploadProfileImg(event.target.files[0]);
  }

  public uploadProfileImg(file: File): void {
    this.uploadService.pushUpload(new Upload(file), (upload: Upload) => {
      this.photoURL.setValue(upload.url);
      this.auth.updateUserData(this.user, this.profileForm.value).then(() => this.isUploading = false);
    });
  }

  public onClickImage(): void {
    this.filePicker.nativeElement.click();
  }
}
