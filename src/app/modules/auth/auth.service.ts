import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Subscription, of } from 'rxjs';
import { switchMap, tap, startWith } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

import { AlertService } from '../shared/services/alert.service';

export interface User {
  uid?: string;
  email?: string | null;
  photoURL?: string;
  displayName?: string;
  role?: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public authInstance: Subscription;
  public user: Observable<User>;

  constructor(
    private af: AngularFireAuth,
    private db: AngularFirestore,
    private alertService: AlertService
  ) {
    this.refreshUser();
  }

  public regularLogin(email: string, password: string): Promise<any> {
    return this.af.auth.signInWithEmailAndPassword(email, password);
  }

  private socialLogin(provider): Promise<any> {
    return this.af.auth.signInWithPopup(provider);
  }

  public signUp(email: string, password: string): Promise<any> {
    return this.af.auth.createUserWithEmailAndPassword(email, password);
  }

  public logout(): Promise<any> {
    return this.af.auth.signOut().then(() => {
      this.alertService.open('Deslogado com sucesso', 'success');
    });
  }

  public googleLogin(): Promise<any> {
    return this.socialLogin(new firebase.auth.GoogleAuthProvider());
  }

  public facebookLogin(): Promise<any> {
    return this.socialLogin(new firebase.auth.FacebookAuthProvider());
  }

  public gitHubLogin(): Promise<any> {
    return this.socialLogin(new firebase.auth.GithubAuthProvider());
  }

  public setDataUser(user): Promise<any> {
    const userRef: AngularFirestoreDocument<User> = this.db.doc(`users/${user.uid}`);
    const data: User = {
      uid: user.uid,
      email: user.email
    };
    localStorage.setItem('user', JSON.stringify(user));
    return userRef.set(data, { merge: true });
  }

  public updateUserData(user: User, data: any): Promise<any> {
    return this.db.doc(`users/${user.uid}`).update(data);
  }

  public refreshUser() {
    this.user = this.af.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.db.doc<User>(`users/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
      }),
      tap(user => localStorage.setItem('user', JSON.stringify(user))),
      startWith(JSON.parse(localStorage.getItem('user')))
    );
  }
}
