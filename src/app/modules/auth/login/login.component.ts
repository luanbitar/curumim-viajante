import { Component, OnInit, OnDestroy } from '@angular/core';
import { Validators, FormBuilder, AbstractControl } from '@angular/forms';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';
import { Router } from '@angular/router';

import 'rxjs/add/operator/take';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import { Subscription } from 'rxjs';

import { AuthService } from '../auth.service';
import { EqualValues } from '../../../utils/equal-values';
import { HandleErrorUtils } from '../../../utils/handle-error.utils';
import { AlertService } from '../../shared/services/alert.service';
import { User } from './../auth.service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [
    trigger('fallInOut', [
      state('', style({
        top: '0',
        opacity: '1',
        position: 'relative'
      })),
      transition('void => *', animate('200ms ease-in', keyframes([
        style({ top: '-100vh', opacity: '0', offset: 0 }),
        style({ top: '0', opacity: '1', offset: 1 })
      ]))),
      transition('* => void', animate('200ms ease-out', keyframes([
        style({ top: '0', opacity: '1', offset: 0 }),
        style({ top: '100vh', opacity: '0', offset: 1 })
      ])))
    ])
  ]
})
export class LoginComponent implements OnInit, OnDestroy {

  public loginForm = this.fb.group({
    email: ['', [
      Validators.required,
      Validators.email
    ]],
    password: ['', Validators.required],
    passwordConfirm: ['', Validators.required]
  }, {
    validator: EqualValues.MatchValues
  });
  public signUp = false;
  public isLoading = false;
  public loginType = 'social';
  private subscriptions = new Subscription();

  constructor(
    public fb: FormBuilder,
    public auth: AuthService,
    private alertService: AlertService,
    private router: Router
  ) {}

  ngOnInit() {}

  public get email(): AbstractControl { return this.loginForm.get('email'); }
  public get password(): AbstractControl { return this.loginForm.get('password'); }
  public get passwordConfirm(): AbstractControl { return this.loginForm.get('passwordConfirm'); }

  public toggleLoginType(to: string): void {
    this.loginForm.reset();
    this.loginType = to;
    if (to === 'email') {
      this.signUp = true;
    } else if (to === 'social') {
      this.signUp = false;
    }
  }

  public login(): void {
    if ((this.signUp && this.loginForm.invalid)
    || (!this.signUp && (this.loginForm.controls.email.invalid || this.loginForm.controls.password.invalid))
    ) {
      return;
    } else if (this.signUp) {
      this.handleResponse(this.auth.signUp(this.loginForm.value.email, this.loginForm.value.password))
      .then(() => this.saveAdditionalUserData());
    } else {
      this.handleResponse(this.auth.regularLogin(this.loginForm.value.email, this.loginForm.value.password));
    }
  }

  // TODO: handler de roles (redirect to client/ or seller/ URL)
  private handleResponse(promise: Promise<any>) {
    this.isLoading = true;
    return promise.then(credentials => {
      this.auth.setDataUser(credentials.user).then((resolve) => {
        this.alertService.open('Logado com sucesso', 'success');
        this.auth.refreshUser();
        this.isLoading = false;
        this.router.navigate(['/client']);
      }).catch(reject => this.isLoading = false);
    }).catch(reject => {
      this.isLoading = false;
      this.alertService.open(HandleErrorUtils.message(reject.code), 'danger');
    });
  }

  private saveAdditionalUserData() {
    const data: User = {
      role: 'client'
    };
    this.subscriptions = this.auth.user
      .subscribe(user => {
        data.photoURL = `${environment.avatarUrl}/avatars/200/${user.uid}`;
        this.auth.updateUserData(user, data);
      });
  }

  ngOnDestroy(): void { this.subscriptions.unsubscribe(); }
}
