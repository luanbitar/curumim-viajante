import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

import { Observable, Observer } from 'rxjs';

import { AuthService } from '../auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private auth: AuthService,
    private router: Router
  ) {}

  // TODO: switch from subscribe to take(1) to prevent multiple subscribers
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      return Observable.create((resolve: Observer<boolean>) => {
        this.auth.user.subscribe(user => {
          if (!!!user) {
            this.router.navigate(['/login']);
          }
          resolve.next(!!user);
          resolve.complete();
        }, reject => {
          resolve.complete();
        });
      });
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.canActivate(route, state);
  }
}
