import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', loadChildren: '../app/modules/auth/auth.module#AuthModule'},
  { path: 'client', loadChildren: '../app/modules/client/client.module#ClientModule'},
  { path: 'seller', loadChildren: '../app/modules/seller/seller.module#SellerModule'},
  { path: '**', redirectTo: 'login', pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
