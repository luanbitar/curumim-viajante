export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyDsmvYlHPN8HxmLaTlou9d9RSQHv6pY33c',
    authDomain: 'curumim-viajante.firebaseapp.com',
    databaseURL: 'https://curumim-viajante.firebaseio.com',
    projectId: 'curumim-viajante',
    storageBucket: 'curumim-viajante.appspot.com',
    messagingSenderId: '703647880654',
    timestampsInSnapshots: true
  },
  avatarUrl: 'https://api.adorable.io',
  placeUrl: 'https://servicodados.ibge.gov.br/api/v1/localidades'
};
